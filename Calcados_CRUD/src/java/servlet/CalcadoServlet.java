/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import controller.FactoryController;
import controller.IController;
import controller.MarcaController;
import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import model.Calcado;
import model.Marca;
import model.Modelo;
/**
 *
 * @author lucas.duffeck
 */
public class CalcadoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CalcadoServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CalcadoServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        
        List<Modelo> modelos = new ArrayList<Modelo>();
        List<Calcado> calcados = new ArrayList<Calcado>();
        IController modeloController = FactoryController.getController("model.Modelo");
        IController calcadoController = FactoryController.getController("model.Calcado");
        
        Calcado calcado = new Calcado();
        if(request.getParameter("id_calcado") != null)
            calcado.setId(Integer.parseInt(request.getParameter("id_calcado")));
        
        String mensagem;
        if(calcado.getId() > 0){
            try{
                if(request.getParameter("excluir").equals("S"))
                    calcadoController.delete(calcado);
                else{
                    calcado = (Calcado)calcadoController.read(calcado);
                    request.setAttribute("calcado", calcado);
                }
            }catch(CalcadoException e){
                mensagem = e.showMessage();
            }catch(Exception e){
                mensagem = "Houve um erro, tente novamente mais tarde!!!";
            }
        }
            
        try{
            modelos = modeloController.findAll();
            calcados = calcadoController.findAll();
        }catch(Exception e){
            e.printStackTrace();
        }
        //marcas.add(marca);
        
        request.setAttribute("modelos", modelos);
        request.setAttribute("calcados", calcados);
        rd = request.getRequestDispatcher("WEB-INF/calcado/cadastrar.jsp");        
        rd.forward(request, response);
        
        //processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Calcado calcado = new Calcado();
        Modelo modelo = new Modelo();
        IController calcadoController = FactoryController.getController(calcado.getClass().getName());
        IController modeloControler = FactoryController.getController(modelo.getClass().getName());
        
        String mensagem;
        try{
            modelo.setId(Integer.parseInt(request.getParameter("modelo")));
            modelo = (Modelo)modeloControler.read(modelo);
            if(request.getParameter("numeracao") != null)
                calcado.setNumeracao(Integer.parseInt(request.getParameter("numeracao")));
            if(request.getParameter("id_atualizar") != null && request.getParameter("id_atualizar") != "")
                calcado.setId(Integer.parseInt(request.getParameter("id_atualizar")));
            calcado.setModelo(modelo);
            if(calcado.getId() > 0)
                calcadoController.update(calcado);             
            else
                calcadoController.insert(calcado);
            mensagem = "Operação Concluída!";
        }catch(CalcadoException e){
            mensagem = e.showMessage();
        }catch(NumberFormatException e){
            mensagem = "Insira a numeração do calçado";
        }catch(Exception e){
            mensagem = "Erro desconhecido";
        }
        
        
        List<Calcado> calcados = new ArrayList<Calcado>();
        IController modeloController = FactoryController.getController("model.Modelo");
        List<Modelo> modelos = new ArrayList<Modelo>();
        try{
            modelos = modeloController.findAll();
            calcados = calcadoController.findAll();
        }catch(Exception e){
            e.printStackTrace();
        }
        //marcas.add(marca);
                
        RequestDispatcher rd;
        request.setAttribute("modelos", modelos);
        request.setAttribute("mensagem", mensagem);
        request.setAttribute("calcados", calcados);
        rd = request.getRequestDispatcher("WEB-INF/calcado/cadastrar.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
