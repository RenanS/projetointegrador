/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author renan.polli
 */
public class FactoryController {
    public static IController getController(String controller) {
    switch (controller) {
            case "model.Calcado":
                return new CalcadoController();
            case "model.Categoria":
                return new CategoriaController();
            case "model.Marca":
                return new MarcaController();
            case "model.Modelo":
                return new ModeloController();
            case "model.TabelaEdicao":
                return new TabelaEdicaoController();
            case "model.TabelaPreco":
                return new TabelaPrecoController();
            case "model.User":
                return new UserController();
        }
        return null;
    }
}
