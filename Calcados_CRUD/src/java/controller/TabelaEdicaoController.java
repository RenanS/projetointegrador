/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.FactoryDAO;
import dao.IDAO;
import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.TabelaEdicao;

/**
 *
 * @author lucas.duffeck
 */
public class TabelaEdicaoController implements IController<TabelaEdicao>{

    @Override
    public void insert(TabelaEdicao object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.insert(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void update(TabelaEdicao object) throws CalcadoException {
       try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.update(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void delete(TabelaEdicao object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.delete(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public TabelaEdicao read(TabelaEdicao object) throws CalcadoException {
         try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.read(object);            
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
        return null;
    }

    @Override
    public List<TabelaEdicao> findAll() throws CalcadoException {
        TabelaEdicao tb = new TabelaEdicao();
        IDAO dao = FactoryDAO.getDAO(tb.getClass().getName());
        return dao.findAll();
    }

    @Override
    public boolean validate(TabelaEdicao object) throws CalcadoException {
        String msg_erro = "";
        if(object == null)
            throw new CalcadoException("A marca não é um objeto válido");
        if(object.getData() == null)
            msg_erro += "A data deve estar preenchida";
        if(object.getUser() == null)
            msg_erro += "O usuário deve estar preenchido";
        if(object.getCalcado() == null)
            msg_erro += "O calcado deve estar preenchido";
        if(!msg_erro.isEmpty())
            throw new CalcadoException(msg_erro);
        
        return true;
    }

    
    
}
