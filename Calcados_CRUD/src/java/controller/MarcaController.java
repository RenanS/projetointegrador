/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.util.List;
import model.Marca;

/**
 *
 * @author lucas.duffeck
 */
public class MarcaController implements IController<Marca>{

    @Override
    public void insert(Marca object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.insert(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void update(Marca object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.update(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void delete(Marca object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.delete(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public Marca read(Marca object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.read(object);            
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
        return null;
    }

    @Override
    public List<Marca> findAll() throws CalcadoException {
        Marca marca = new Marca();
        IDAO dao = FactoryDAO.getDAO(marca.getClass().getName());
        return dao.findAll();
    }

    @Override
    public boolean validate(Marca object) throws CalcadoException {
        String msg_erro = "";
        if(object == null)
            throw new CalcadoException("A marca não é um objeto válido");
        if(object.getNome() == null)
            msg_erro += "O nome da marca deve estar preenchido";
        if(!msg_erro.isEmpty())
            throw new CalcadoException(msg_erro);
        
        return true;
    }
}
