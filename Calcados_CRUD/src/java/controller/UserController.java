/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import model.User;

/**
 *
 * @author lucas.duffeck
 */
public class UserController implements IController<User>{

    @Override
    public void insert(User object) throws CalcadoException {
        try{
            object.setSenha(criptografar(object.getSenha()));
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.insert(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void update(User object) throws CalcadoException {
         try{
            object.setSenha(criptografar(object.getSenha()));
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.update(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void delete(User object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.delete(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public User read(User object) throws CalcadoException {
        try{
            IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
            return (User)dao.read(object);
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public List<User> findAll() throws CalcadoException {
        User user = new User();
        IDAO dao = FactoryDAO.getDAO(user.getClass().getName());
        return dao.findAll();
    }
    
    @Override
    public boolean validate(User object) throws CalcadoException {
        String msg_erro = "";
        if(object == null)
            throw new CalcadoException("O usuário não é um objeto válido");
        if(object.getNome() == null ||object.getNome().isEmpty())
            msg_erro += "O nome do usuário deve estar preenchido!\n";
        if(object.getLogin() == null ||object.getLogin().isEmpty())
            msg_erro += "O login do usuário deve estar preenchido!\n";
        if(object.getSenha() == null ||object.getSenha().isEmpty())
            msg_erro += "A senha do usuário deve estar preenchido!\n";
        if(object.getEmail() == null ||object.getLogin().isEmpty())
            msg_erro += "O email do usuário deve estar preenchido!\n";
        if(!msg_erro.isEmpty())
            throw new CalcadoException(msg_erro);
        
        return true;
    }
    
    public String criptografar(String senha) throws NoSuchAlgorithmException{
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(senha.getBytes(),0,senha.length());
        return new BigInteger(1,m.digest()).toString(16);     
    }
    
    public boolean efetuarLogin(User object) throws CalcadoException, NoSuchAlgorithmException{
        object.setSenha(criptografar(object.getSenha()));
        if(object.getLogin() == null || object.getSenha() == null)
            return false;
        
        User user = read(object);
        if(user == null)
            return false;
        return user.getLogin().equals(object.getLogin()) && user.getSenha().equals(object.getSenha());
    }
}
