/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.util.List;
import model.Categoria;

/**
 *
 * @author lucas.duffeck
 */
public class CategoriaController implements IController<Categoria>{

    @Override
    public void insert(Categoria object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.insert(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void update(Categoria object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.update(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void delete(Categoria object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.delete(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public Categoria read(Categoria object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.read(object);            
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
        return null;
    }

    @Override
    public List<Categoria> findAll() throws CalcadoException {
        Categoria categoria = new Categoria();
        IDAO dao = FactoryDAO.getDAO(categoria.getClass().getName());
        return dao.findAll();
    }

    @Override
    public boolean validate(Categoria object) throws CalcadoException {
        String msg_erro = "";
        if(object == null)
            throw new CalcadoException("<br /> A categoria não é um objeto válido");
        if(object.getNome().isEmpty())
            msg_erro += "<br /> O nome da categoria deve estar preenchido";
        if(object.getDescricao().isEmpty())
            msg_erro += "<br /> A descrição da categoria deve estar preenchida";
        if(!msg_erro.isEmpty())
            throw new CalcadoException(msg_erro);
        
        return true;
    }

    
    
}
