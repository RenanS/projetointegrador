/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.util.List;
import model.Calcado;
import model.Modelo;

/**
 *
 * @author lucas.duffeck
 */
public class CalcadoController implements IController<Calcado> {

    @Override
    public void insert(Calcado object) throws CalcadoException {
        try {
            if (validate(object)) {
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.insert(object);
            }
        } catch (CalcadoException e) {
            throw e;
        } catch (Exception e) {
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void update(Calcado object) throws CalcadoException {
        try {
            if (validate(object)) {
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.update(object);
            }
        } catch (CalcadoException e) {
            throw e;
        } catch (Exception e) {
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void delete(Calcado object) throws CalcadoException {
        try {
            IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
            dao.delete(object);
        } catch (CalcadoException e) {
            throw e;
        } catch (Exception e) {
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public Calcado read(Calcado object) throws CalcadoException {
        try {
            IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
            Calcado calcado = (Calcado) dao.read(object);
            Modelo modelo = new Modelo();
            IController modeloController = FactoryController.getController("model.Modelo");
            modelo.setId(calcado.getModelo().getId());
            calcado.setModelo(modelo);
            return calcado;
        } catch (CalcadoException e) {
            throw e;
        } catch (Exception e) {
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public List<Calcado> findAll() throws CalcadoException {
        Calcado calcado = new Calcado();
        IDAO dao = FactoryDAO.getDAO(calcado.getClass().getName());
        List<Calcado> calcados = dao.findAll();
        Modelo modelo;
        IController modeloController = FactoryController.getController("model.Modelo");
        for (Calcado aux : calcados) {
            modelo = new Modelo();
            modelo.setId(aux.getModelo().getId());
            modelo = (Modelo) modeloController.read(modelo);
            aux.setModelo(modelo);
        }
        return calcados;
    }

    @Override
    public boolean validate(Calcado object) throws CalcadoException {
        String msg_erro = "";
        if (object == null) {
            throw new CalcadoException("O calçado não é um objeto válido\n");
        }
        if (object.getModelo() == null) {
            msg_erro += "O modelo do calçado deve estar preenchido\n";
        }
        if (new Integer(object.getNumeracao()) == null) {
            msg_erro += "A numeração calçado deve estar preenchido\n";
        }
        //if(object.getUser()== null)
        //    msg_erro += "Houve um erro ao identificar o usuário ativo\n";
        if (!msg_erro.isEmpty()) {
            throw new CalcadoException(msg_erro);
        }

        return true;
    }

}
