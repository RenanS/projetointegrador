/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.util.List;
import model.Modelo;

/**
 *
 * @author lucas.duffeck
 */
public class ModeloController implements IController<Modelo>{

    @Override
    public void insert(Modelo object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.insert(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void update(Modelo object) throws CalcadoException {
         try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.update(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void delete(Modelo object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.delete(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public Modelo read(Modelo object) throws CalcadoException {
        try{
            IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
            return (Modelo)dao.read(object);
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public List<Modelo> findAll() throws CalcadoException {
        Modelo modelo = new Modelo();
        IDAO dao = FactoryDAO.getDAO(modelo.getClass().getName());
        return dao.findAll();
    }

    @Override
    public boolean validate(Modelo object) throws CalcadoException {
        String msg_erro = "";
        if(object == null)
            throw new CalcadoException("O modelo não é um objeto válido");
        if(object.getNome() == null)
            msg_erro += "O nome do modelo deve estar preenchido\n";
        if(object.getCod() == null)
            msg_erro += "O código do modelo deve estar preenchido\n";
        if(object.getCor() == null)
            msg_erro += "A cor do modelo deve estar preenchida\n";
        if(object.getDescricao() == null)
            msg_erro += "A descrição do modelo deve estar preenchida\n";
        if(object.getCategoria() == null)
            msg_erro += "A categoria do modelo deve estar preenchida\n";
        if(object.getMarca() == null)
            msg_erro += "A marca do modelo deve estar preenchida\n";
        if(!msg_erro.isEmpty())
            throw new CalcadoException(msg_erro);
        
        return true;
    }    
}
