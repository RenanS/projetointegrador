/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.util.List;
import model.TabelaPreco;

/**
 *
 * @author lucas.duffeck
 */
public class TabelaPrecoController implements IController<TabelaPreco>{

    @Override
    public void insert(TabelaPreco object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.insert(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void update(TabelaPreco object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.update(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public void delete(TabelaPreco object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.delete(object);
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
    }

    @Override
    public TabelaPreco read(TabelaPreco object) throws CalcadoException {
        try{
            if(validate(object)){
                IDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.read(object);            
            }
        }catch(CalcadoException e){
            throw e;
        }catch(Exception e){
            throw new CalcadoException("Erro desconhecido!");
        }
        return null;
    }

    @Override
    public List<TabelaPreco> findAll() throws CalcadoException {
        TabelaPreco tb = new TabelaPreco();
        IDAO dao = FactoryDAO.getDAO(tb.getClass().getName());
        return dao.findAll();
    }

    @Override
    public boolean validate(TabelaPreco object) throws CalcadoException {
        String msg_erro = "";
        if(object == null)
            throw new CalcadoException("A tabela de preços não é um objeto válido");
        if(object.getData() == null)
            msg_erro += "A data deve estar preenchida";
        if(object.getPreco() == 0)    
            msg_erro += "A marca deve estar preenchida";
        if(object.getMarca() == null)
            msg_erro += "A marca deve estar preenchida";
        if(object.getModelo() == null)
            msg_erro += "O modelo deve estar preenchido";
        if(!msg_erro.isEmpty())
            throw new CalcadoException(msg_erro);
        
        return true;
    }

}
