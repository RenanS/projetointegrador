/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 *
 * @author renan.polli
 */
public class DBConnection {

    private static final String ARQ_CONF = "resources/conexao.conf";
    private static DBConnection instancia = new DBConnection();
    //private static Properties configuracoes;

    private static String driver = "com.mysql.jdbc.Driver";
    private static String url = "jdbc:mysql://localhost:3306/calcados";
    private static String usuario = "root";
    private static String senha = "root";

    /**
     * Construtor Privado para o padrÃ£o singleton
     */
    private DBConnection() {
    }

    //////////////////////////////////////////////////////
    // METODOS ESTATICOS
    //////////////////////////////////////////////////////
    /**
     * Metodo obrigatorio para o padrao Singleton
     *
     * @return
     */
    public static DBConnection getInstance() {
        return instancia;
    }

    /**
     * Metodo que obtem a instancia de Connection com o BD
     *
     * @return
     */
    public static Connection getConnection() {
        return DBConnection.getInstance().obterDBConnection();
    }

    /**
     * Metodo responsavel por fechar um ResultSet
     *
     * @param rs
     */
    public static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception excp) {
                // Ignore, we don't care, we do care when we open it
            }
        }
    }

    /**
     * Metodo responsavel por fechar uma Connection
     *
     * @param cn
     */
    public static void close(Connection cn) {
        if (cn != null) {
            try {
                cn.close();
            } catch (Exception excp) {
                // Ignore, we don't care, we do care when we open it
            }
        }
    }

    /**
     * Metodo responsavel por fechar uma Statement
     *
     * @param stmt
     */
    public static void close(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception excp) {
                // Ignore, we don't care, we do care when we open it
            }
        }
    }

    /**
     * Metodo responsavel por fechar ResultSet, Statement e Connection
     * realizando um overload de metodos
     *
     * @param rs
     * @param ps
     * @param conn
     */
    public static void close(ResultSet rs, Statement ps, Connection conn) {
        close(rs);
        close(ps);
        close(conn);
    }

    //////////////////////////////////////////////////////
    // METODOS COMUNS
    //////////////////////////////////////////////////////
    /**
     * Metodo que realiza a leitura das configuraÃ§Ãµes do arquivo de properties
     */
    /*
    private void lerConfiguracoes() {
        try {
            File arqConf = new File(ARQ_CONF);
            if (!arqConf.exists()) {
                throw new IllegalArgumentException(
                        "Arquivo de configuraÃ§Ã£o nÃ£o existe " + ARQ_CONF);
            }
            FileInputStream entrada = new FileInputStream(arqConf);
            configuracoes = new Properties();
            configuracoes.load(entrada);
            entrada.close();
        } catch (IOException e) {
            throw new RuntimeException("Erro de E/S", e);
        }
    }
*/
    /**
     * Metodo responsavel por obter a instancia da classe connection do BD
     *
     * @return
     */
    public Connection obterDBConnection() {
        try {
            Class.forName(driver);
            Connection con = DriverManager.getConnection(url, usuario, senha);
            return con;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Classe nÃ£o encontrada. "
                    + "Verifique driver no classpath", e);
        } catch (SQLException e) {
            throw new RuntimeException("Erro SQL", e);
        }
    }
}
