/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Marca;

/**
 *
 * @author lucas.duffeck
 */
public class MarcaDAO implements IDAO<Marca>{

    @Override
    public void insert(Marca object) throws CalcadoException {
        int cont = 1;
        String sql = "INSERT INTO `calcados`.`marca`\n" +
            " (`nome`)\n" +
            " VALUES\n" +
            " (?);";

        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(cont++, object.getNome());

                ps.executeUpdate();
                //Obtem o ID gerado pelo banco HSQLDB
                //object.setId(retrievePrimaryKeygenerated(rs, ps));
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
		//return object.getId();
    }

    @Override
    public void update(Marca object) throws CalcadoException {
        String sql;
        sql = "UPDATE `calcados`.`marca`"+
            "SET"+
            "`nome` = ?"+
            "WHERE `id` = ?";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setString(cont++, object.getNome());
                ps.setLong(cont++, object.getId());

                ps.executeUpdate();
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(null, ps, connection);
        }
    }

    @Override
    public void delete(Marca object) throws CalcadoException {
        String sql;
        sql="DELETE FROM `calcados`.`marca`\n" +
            "WHERE `id` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setObject(cont++, object.getId());

                ps.executeUpdate();
        } catch (SQLException e) {
               throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public Marca read(Marca object) throws CalcadoException {
        String sql;
        sql="SELECT * " +
            " FROM `calcados`.`marca` WHERE TRUE ";
        if(object.getId() != 0)
            sql += " AND `id` = " + object.getId();
        if(!object.getNome().isEmpty())
            sql += " AND `nome` = " + object.getNome();
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
         try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                if(rs.next()){
                    Marca aux = new Marca();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    return aux;
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return null;
    }

    @Override
    public List<Marca> findAll() throws CalcadoException {
        String sql;
        sql="SELECT marca.id, marca.nome FROM calcados.marca;";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Marca> marcas = new ArrayList<Marca>();
        try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()){
                    Marca aux = new Marca();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    marcas.add(aux);
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return marcas;
    }
    
}
