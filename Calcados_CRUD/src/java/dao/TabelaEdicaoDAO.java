/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Calcado;
import model.TabelaEdicao;
import model.User;

/**
 *
 * @author lucas.duffeck
 */
public class TabelaEdicaoDAO implements IDAO<TabelaEdicao>{

    @Override
    public void insert(TabelaEdicao object) throws CalcadoException {
        int cont = 1;
        String sql = "INSERT INTO `calcados`.`tabelaedicao`\n" +
                    "(`Calcado_id`,\n" +
                    "`User_id`,\n" +
                    "`data`)\n" +
                    "VALUES\n" +
                    "(?,\n" +
                    "?,\n" +
                    "?);";

        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(cont++, object.getCalcado().getId());
                ps.setInt(cont++, object.getUser().getId());
                //ps.setDate(cont++, object.getData());

                ps.executeUpdate();
                //Obtem o ID gerado pelo banco HSQLDB
                //object.setId(retrievePrimaryKeygenerated(rs, ps));
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public void update(TabelaEdicao object) throws CalcadoException {
        String sql;
        sql = "UPDATE `calcados`.`tabelaedicao`\n" +
                "SET\n" +
                "`Calcado_id` = ?,\n" +
                "`User_id` = ?,\n" +
                "`data` = ?\n" +
                "WHERE `data` = ?";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setInt(cont++, object.getCalcado().getId());
                ps.setInt(cont++, object.getUser().getId());
                //ps.setDate(cont++, object.getData());

                ps.executeUpdate();
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(null, ps, connection);
        }
    }

    @Override
    public void delete(TabelaEdicao object) throws CalcadoException {
        String sql;
        sql="DELETE FROM `calcados`.`tabelaedicao`\n" +
            "WHERE `data` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setObject(cont++, object.getData());

                ps.executeUpdate();
        } catch (SQLException e) {
               throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public TabelaEdicao read(TabelaEdicao object) throws CalcadoException {
        String sql;
        sql="SELECT * " +
            " FROM `calcados`.`tabelaedicao` WHERE TRUE ";
        if(object.getCalcado()!= null)
            sql += " AND `Calcado_id` = " + object.getCalcado().getId();
        if(object.getData() != null)
            sql += " AND `data` = " + new java.sql.Date(object.getData().getTime());
        if(object.getUser() != null)
            sql += " AND `User_id` = " + object.getUser();
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
         try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                if(rs.next()){
                    Calcado calcado = new Calcado();
                    calcado.setId(rs.getInt("Calcado_id"));
                    
                    User user = new User();
                    user.setId(rs.getInt("User_id"));
                    
                    TabelaEdicao aux = new TabelaEdicao(); 
                    aux.setUser(user);
                    aux.setCalcado(calcado);
                    aux.setData(rs.getDate("data"));
                    return aux;
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return null;
    }

    @Override
    public List<TabelaEdicao> findAll() throws CalcadoException {
        String sql;
        sql="SELECT `tabelaedicao`.`Calcado_id`,\n" +
            "    `tabelaedicao`.`User_id`,\n" +
            "    `tabelaedicao`.`data`\n" +
            "FROM `calcados`.`tabelaedicao`;";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TabelaEdicao> tbs = new ArrayList<TabelaEdicao>();
        try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()){
                    TabelaEdicao aux = new TabelaEdicao();
                    aux.setData(rs.getDate("data"));  
                    //aux.setCalcado(rs.getString("nome"));
                    //aux.setUser();
                    tbs.add(aux);
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return tbs;
    }
    
}
