/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.lang.reflect.Constructor;
import model.*;

/**
 *
 * @author lucas.duffeck
 */
public class FactoryDAO {

    public static IDAO getDAO(String dao) {
        switch (dao) {
            case "model.Calcado":
                //return Class.forName(dao);
                return new CalcadoDAO();
            case "model.Categoria":
                return new CategoriaDAO();
            case "model.Marca":
                return new MarcaDAO();
            case "model.Modelo":
                return new ModeloDAO();
            case "model.TabelaEdicao":
                return new TabelaEdicaoDAO();
            case "model.TabelaPreco":
                return new TabelaPrecoDAO();
            case "model.User":
                return new UserDAO();
        }
        return null;
    }
}
