/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import controller.IController;
import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Calcado;
import model.Modelo;

/**
 *
 * @author lucas.duffeck
 */
public class CalcadoDAO implements IDAO<Calcado> {

    @Override
    public void insert(Calcado object) throws CalcadoException {
        int cont = 1;
        String sql = "INSERT INTO calcados.calcado "
                + " (numeracao, "
                + " Modelo_id) "
                + " VALUES"
                + " (?, "
                + "?);";

        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(cont++, object.getNumeracao());
            ps.setInt(cont++, object.getModelo().getId());

            ps.executeUpdate();
            //Obtem o ID gerado pelo banco HSQLDB
            //object.setId(retrievePrimaryKeygenerated(rs, ps));
        } catch (SQLException e) {
            throw new CalcadoException(e.toString());
        } finally {
            DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public void update(Calcado object) throws CalcadoException {
        String sql;
        sql = "UPDATE calcados.calcado "
                + " SET "
                + " numeracao = ?,"
                + " Modelo_id = ?"
                + " WHERE id = ?;";

        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(cont++, object.getNumeracao());
            ps.setInt(cont++, object.getModelo().getId());
            ps.setInt(cont++, object.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new CalcadoException("Erro de SQL");
        } finally {
            DBConnection.close(null, ps, connection);
        }
    }

    @Override
    public void delete(Calcado object) throws CalcadoException {
        String sql;
        sql = "DELETE FROM calcados.calcado "
                + " WHERE id = ?;";

        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setObject(cont++, object.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new CalcadoException("Erro de SQL");
        } finally {
            DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public Calcado read(Calcado object) throws CalcadoException {
        String sql;
        sql = "SELECT calcado.id, "
                + "    calcado.numeracao, "
                + "    calcado.Modelo_id "
                + " FROM calcados.calcado WHERE TRUE ";
        if (object.getId() != 0) {
            sql += " AND id = " + object.getId();
        }
        if (object.getNumeracao() != 0) {
            sql += " AND numeracao = " + object.getNumeracao();
        }
        if (object.getModelo() != null) {
            sql += " AND Modelo_id = " + object.getModelo().getId();
        }

        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                Calcado aux = new Calcado();
                aux.setId(rs.getInt("id"));
                aux.setNumeracao(rs.getInt("numeracao"));
                Modelo modelo = new Modelo();
                modelo.setId(rs.getInt("Modelo_id"));
                aux.setModelo(modelo);
                return aux;
            }
        } catch (SQLException e) {
            throw new CalcadoException("Erro de SQL");
        } finally {
            DBConnection.close(rs, ps, connection);
        }
        return null;
    }

    @Override
    public List<Calcado> findAll() throws CalcadoException {
        String sql;
        sql = "SELECT calcado.id, "
                + "    calcado.numeracao, "
                + "    calcado.Modelo_id "
                + "FROM calcados.calcado;";

        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Calcado> calcados = new ArrayList<Calcado>();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Calcado aux = new Calcado();
                aux.setId(rs.getInt("id"));
                aux.setNumeracao(rs.getInt("numeracao"));
                Modelo modelo = new Modelo();
                modelo.setId(rs.getInt("Modelo_id"));
                aux.setModelo(modelo);
                calcados.add(aux);
            }
        } catch (SQLException e) {
            throw new CalcadoException("Erro de SQL");
        } finally {
            DBConnection.close(rs, ps, connection);
        }
        return calcados;
    }

}
