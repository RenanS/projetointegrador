/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Marca;
import model.Modelo;
import model.TabelaPreco;

/**
 *
 * @author lucas.duffeck
 */
public class TabelaPrecoDAO implements IDAO<TabelaPreco>{

    @Override
    public void insert(TabelaPreco object) throws CalcadoException {
        int cont = 1;
        String sql = "INSERT INTO `calcados`.`tabelapreco` " +
                    "(`TabelaPreco_id`, " +
                    "`Modelo_id`, " +
                    "`data`, " +
                    "`preco`) " +
                    "VALUES " +
                    "(?, " +
                    "?, " +
                    "?, " +
                    "?);";

        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(cont++, object.getMarca().getId());
                ps.setInt(cont++, object.getModelo().getId());
                //ps.setDate(cont++, );
                ps.setFloat(cont++, object.getPreco());

                ps.executeUpdate();
                //Obtem o ID gerado pelo banco HSQLDB
                //object.setId(retrievePrimaryKeygenerated(rs, ps));
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public void update(TabelaPreco object) throws CalcadoException {
        String sql;
        sql = "UPDATE `calcados`.`tabelapreco` " +
                "SET " +
                "`preco` = ? " +
                "WHERE `TabelaPreco_id` = ? AND `Modelo_id` = ? AND `data` = ?;";

        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setFloat(cont++, object.getPreco());
                ps.executeUpdate();
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(null, ps, connection);
        }
    }

    @Override
    public void delete(TabelaPreco object) throws CalcadoException {
        String sql;
        sql="DELETE FROM `calcados`.`tabelapreco` " +
"           WHERE `id` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql);
                //ps.setObject(cont++, object.getDate());

                ps.executeUpdate();
        } catch (SQLException e) {
               throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public TabelaPreco read(TabelaPreco object) throws CalcadoException {
        String sql;
        sql="SELECT * " +
            " FROM `calcados`.`tabelapreco` WHERE TRUE ";
        if(object.getMarca()!= null)
            sql += " AND `Marca_id` = " + object.getMarca().getId();
        if(object.getModelo() != null)
            sql += " AND Modelo_id` = " + object.getModelo().getId();
        if(object.getData()!= null)
            sql += " AND `data` = " + new java.sql.Date(object.getData().getTime());
        if(object.getPreco() != 0)
            sql += " AND `preco` = " + object.getPreco();
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
         try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                if(rs.next()){
                    Marca marca = new Marca();
                    marca.setId(rs.getInt("Marca_id"));
                    
                    Modelo modelo = new Modelo();
                    modelo.setId(rs.getInt("Modelo_id"));
                    
                    TabelaPreco aux = new TabelaPreco();
                    aux.setMarca(marca);  
                    aux.setModelo(modelo);
                    aux.setData(rs.getDate("data"));
                    aux.setPreco(rs.getFloat("preco"));
                    return aux;
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return null;
    }

    @Override
    public List<TabelaPreco> findAll() throws CalcadoException {
        String sql;
        sql="SELECT `tabelapreco`.`Marca_id`, " +
            "    `tabelapreco`.`Modelo_id`, " +
            "    `tabelapreco`.`data`, " +
            "    `tabelapreco`.`preco` " +
            "FROM `calcados`.`tabelapreco`;";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TabelaPreco> tbs = new ArrayList<TabelaPreco>();
        try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()){
                    TabelaPreco aux = new TabelaPreco();
                    //aux.setMarca(null);
                    //aux.setModelo(null);
                    aux.setData(rs.getDate("data"));
                    aux.setPreco(rs.getFloat("preco"));
                    tbs.add(aux);
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return tbs;
    }
    
}
