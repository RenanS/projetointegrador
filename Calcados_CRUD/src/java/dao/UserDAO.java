/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 *
 * @author lucas.duffeck
 */
public class UserDAO implements IDAO<User>{

    @Override
    public void insert(User object) throws CalcadoException {
         int cont = 1;
        String sql = "INSERT INTO `calcados`.`user`\n" +
                    "(`nome`,\n" +
                    "`login`,\n" +
                    "`senha`,\n" +
                    "`email`)\n" +
                    "VALUES\n" +
                    "(?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?);";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(cont++, object.getNome());
                ps.setString(cont++, object.getLogin());
                ps.setString(cont++, object.getSenha());
                ps.setString(cont++, object.getEmail());

                ps.executeUpdate();
                //Obtem o ID gerado pelo banco HSQLDB
                //object.setId(retrievePrimaryKeygenerated(rs, ps));
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
		//return object.getId();
    }

    @Override
    public void update(User object) throws CalcadoException {
        String sql;
        sql = "UPDATE `calcados`.`user` " +
            "SET " +
            "`nome` = ?, " +
            "`login` = ?, " +
            "`senha` = ?, " +
            "`email` = ? " +
            "WHERE `id` = '"+object.getId()+"'";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setString(cont++, object.getNome());
                ps.setString(cont++, object.getLogin());
                ps.setString(cont++, object.getSenha());
                ps.setString(cont++, object.getEmail());

                ps.executeUpdate();
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(null, ps, connection);
        }
    }

    @Override
    public void delete(User object) throws CalcadoException {
        String sql;
        sql="DELETE FROM `calcados`.`user` " +
            "WHERE `id` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setObject(cont++, object.getId());

                ps.executeUpdate();
        } catch (SQLException e) {
               throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public User read(User object) throws CalcadoException {
        String sql;
        sql="SELECT `user`.`id`, " +
            "    `user`.`nome`, " +
            "    `user`.`login`, " +
            "    `user`.`senha`, " +
            "    `user`.`email` " +
            "FROM `calcados`.`user` WHERE TRUE ";
        if(object.getId() != 0)
            sql += " AND `id` = " + object.getId();
        if(object.getNome() != null)
            sql += " AND `nome` = " + object.getNome();
        if(object.getLogin() != null)
            sql += " AND `login` = '" + object.getLogin() + "'";
        if(object.getEmail() != null)
            sql += " AND `email` = " + object.getEmail();
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
         try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                if(rs.next()){
                    User aux = new User();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    aux.setLogin(rs.getString("login"));
                    aux.setSenha(rs.getString("senha"));
                    aux.setEmail(rs.getString("email"));
                    return aux;
                }
        } catch (SQLException e) {
                throw new CalcadoException(e.getMessage());
                //throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
         return null;
    }

    @Override
    public List<User> findAll() throws CalcadoException {
        String sql;
        sql="SELECT `user`.`id`, " +
            "    `user`.`nome`, " +
            "    `user`.`login`, " +
            "    `user`.`senha`, " +
            "    `user`.`email` " +
            "FROM `calcados`.`user`;";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<User>();
        try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()){
                    User aux = new User();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    aux.setLogin(rs.getString("login"));
                    aux.setSenha(rs.getString("senha"));
                    aux.setEmail(rs.getString("email"));
                    users.add(aux);
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return users;
    }

   
}
