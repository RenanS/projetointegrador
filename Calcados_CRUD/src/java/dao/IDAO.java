/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import exception.CalcadoException;
import java.util.List;

/**
 *
 * @author lucas.duffeck
 */
public interface IDAO <T>{
    
    public void insert(T object) throws CalcadoException;
    public void update(T object) throws CalcadoException;
    public void delete(T object) throws CalcadoException;;
    public T read(T object) throws CalcadoException;;
    public List<T> findAll() throws CalcadoException;
    
    
}
