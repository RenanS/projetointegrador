/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Categoria;
import model.Modelo;

/**
 *
 * @author lucas.duffeck
 */
public class ModeloDAO implements IDAO<Modelo>{

    @Override
    public void insert(Modelo object) throws CalcadoException {
        int cont = 1;
        String sql = "INSERT INTO `calcados`.`modelo`\n" +
                    "(`nome`,\n" +
                    "`codigo`,\n" +
                    "`cor`,\n" +
                    "`descricao`,\n" +
                    "`Categoria_id`)\n" +
                    "VALUES\n" +
                    "(?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?,\n" +
                    "?);";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(cont++, object.getNome());
                ps.setString(cont++, object.getCod());
                ps.setString(cont++, object.getCor());
                ps.setString(cont++, object.getDescricao());
                ps.setInt(cont++, object.getCategoria().getId());

                ps.executeUpdate();
                //Obtem o ID gerado pelo banco HSQLDB
                //object.setId(retrievePrimaryKeygenerated(rs, ps));
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
                //throw new C("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public void update(Modelo object) throws CalcadoException {
       String sql;
        sql = "UPDATE `calcados`.`modelo`\n" +
            "SET\n" +
            "`nome` = ?,\n" +
            "`codigo` = ?,\n" +
            "`cor` = ?,\n" +
            "`descricao` = ?,\n" +
            "`Categoria_id` = ?\n" +
            "WHERE `id` = ? AND `Categoria_id` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setLong(cont++, object.getId());
                ps.setString(cont++, object.getNome());
                ps.setString(cont++, object.getCod());
                ps.setString(cont++, object.getCor());
                ps.setString(cont++, object.getDescricao());
                ps.setInt(cont++, object.getCategoria().getId());

                ps.executeUpdate();
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(null, ps, connection);
        }
    }

    @Override
    public void delete(Modelo object) throws CalcadoException {
        String sql;
        sql="DELETE FROM `calcados`.`modelo`\n" +
            "WHERE `id` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setObject(cont++, object.getId());

                ps.executeUpdate();
        } catch (SQLException e) {
               throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public Modelo read(Modelo object) throws CalcadoException {
        String sql;
        sql="SELECT * " +
            " FROM calcados.modelo WHERE TRUE ";
        if(object.getId() != 0)
            sql += " AND id = " + object.getId();
        if(object.getNome() != null)
            sql += " AND nome = " + object.getNome();
        if(object.getCategoria() != null)
            sql += " AND id_categoria = " + object.getCategoria();
        if(object.getCod() != null)
            sql += " AND cod = " + object.getCod();
        if(object.getCor() != null)
            sql += " AND cor = " + object.getCor();
        if(object.getDescricao() != null)
            sql += " AND descricao = " + object.getDescricao();
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
         try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                if(rs.next()){
                    Modelo aux = new Modelo();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    aux.setCod(rs.getString("codigo"));
                    aux.setCor(rs.getString("cor"));
                    aux.setDescricao(rs.getString("descricao"));
                    return aux;
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return null;
    }

    @Override
    public List<Modelo> findAll() throws CalcadoException {
        String sql;
        sql="SELECT `modelo`.`id`,\n" +
            "    `modelo`.`nome`,\n" +
            "    `modelo`.`codigo`,\n" +
            "    `modelo`.`cor`,\n" +
            "    `modelo`.`descricao`,\n" +
            "    `modelo`.`Categoria_id`\n" +
            "FROM `calcados`.`modelo`;";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Modelo> modelos = new ArrayList<Modelo>();
        try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()){
                    Modelo aux = new Modelo();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    aux.setCod(rs.getString("codigo"));
                    aux.setCor(rs.getString("cor"));
                    aux.setDescricao(rs.getString("descricao"));
                    //aux.setCategoria(rs.getObject(columnIndex));
                    modelos.add(aux);
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return modelos;
    }
    
}
