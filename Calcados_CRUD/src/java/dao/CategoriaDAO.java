/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.connection.DBConnection;
import exception.CalcadoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Calcado;
import model.Categoria;

/**
 *
 * @author lucas.duffeck
 */
public class CategoriaDAO implements IDAO<Categoria>{

    @Override
    public void insert(Categoria object) throws CalcadoException {
        int cont = 1;
        String sql = "INSERT INTO `calcados`.`categoria` " +
                    "(`nome`, " +
                    "`descricao`) " +
                    "VALUES " +
                    "(?, " +
                    "?);";

        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(cont++, object.getNome());
                ps.setString(cont++, object.getDescricao());

                ps.executeUpdate();
                //Obtem o ID gerado pelo banco HSQLDB
                //object.setId(retrievePrimaryKeygenerated(rs, ps));
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
		//return object.getId();
    }

    @Override
    public void update(Categoria object) throws CalcadoException {
        String sql;
        sql = "UPDATE `calcados`.`categoria` " +
            "SET " +
            "`id` = ?, " +
            "`nome` = ?, " +
            "`descricao` = ? " +
            "WHERE `id` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setLong(cont++, object.getId());
                ps.setString(cont++, object.getNome());
                ps.setString(cont++, object.getDescricao());

                ps.executeUpdate();
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(null, ps, connection);
        }
    }

    @Override
    public void delete(Categoria object) throws CalcadoException {
        String sql;
        sql="DELETE FROM `calcados`.`categoria` " +
"           WHERE `id` = ?;";
        
        int cont = 1;
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                ps = connection.prepareStatement(sql);
                ps.setObject(cont++, object.getId());

                ps.executeUpdate();
        } catch (SQLException e) {
               throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
    }

    @Override
    public Categoria read(Categoria object) throws CalcadoException {
        String sql;
        sql="SELECT * " +
            " FROM `calcados`.`calcado` WHERE TRUE ";
        if(object.getId() != 0)
            sql += " AND `id` = " + object.getId();
        if(!object.getDescricao().isEmpty())
            sql += " AND `descricao` = " + object.getDescricao();
        if(object.getNome()!= null)
            sql += " AND `nome` = " + object.getNome();
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
         try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                if(rs.next()){
                    Categoria aux = new Categoria();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    aux.setDescricao(rs.getString("descricao"));
                    return aux;
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return null;
    }

    @Override
    public List<Categoria> findAll() throws CalcadoException {
        String sql;
        sql="SELECT `categoria`.`id`, " +
            "    `categoria`.`nome`, " +
            "    `categoria`.`descricao` " +
            "FROM `calcados`.`categoria`;";
        
        Connection connection = DBConnection.getInstance().obterDBConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Categoria> categorias = new ArrayList<Categoria>();
        try {
                ps = connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while(rs.next()){
                    Categoria aux = new Categoria();
                    aux.setId(rs.getInt("id"));  
                    aux.setNome(rs.getString("nome"));
                    aux.setDescricao(rs.getString("descricao"));
                    categorias.add(aux);
                }
        } catch (SQLException e) {
                throw new CalcadoException("Erro de SQL");
        } finally {
                DBConnection.close(rs, ps, connection);
        }
        return categorias;
    }

   
    
}
