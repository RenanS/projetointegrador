
import controller.MarcaController;
import controller.UserController;
import dao.FactoryDAO;
import dao.IDAO;
import exception.CalcadoException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author renan.polli
 */
public class Principal {
    
    public static void main(String[] args){
        
        
        Calcado calcado = new Calcado();
        Categoria categoria = new Categoria();
        Marca marca = new Marca();
        User user = new User();
        
        IDAO dao = FactoryDAO.getDAO(marca.getClass().getName());
        System.out.println(dao.getClass().getName());
        
        dao = FactoryDAO.getDAO(categoria.getClass().getName());
        System.out.println(dao.getClass().getName());
        
        dao = FactoryDAO.getDAO(calcado.getClass().getName());
        System.out.println(dao.getClass().getName());
        
        
        
        MarcaController mc = new MarcaController();
        /*try{
            marca.setModelo(new ArrayList<Modelo>());
            //marca.setNome("DC");
            mc.insert(marca);
        }catch(Exception e){
            e.printStackTrace();
        }*/
        
        try{
            List<Marca> marcas = mc.findAll();
            for(int i = 0; i < marcas.size(); i++){
                System.out.println(marcas.get(i).getNome()+" "+marcas.get(i).getId());
            }
                
        }catch(Exception e){
            e.printStackTrace();
        } 
        
        UserController uc = new UserController();
        /*user.setNome("Pedro Souza");
        user.setEmail("pedro@gmail.com");
        user.setLogin("pedroSouza");
        user.setSenha("pedrosouza");
        user.setId(3);
        try{
            uc.update(user);
        }catch(Exception e){
            e.printStackTrace();
        }*/
        
        try{
            List<User> users = uc.findAll();
            for(int i = 0; i < users.size(); i++){
                System.out.println(users.get(i).getNome()+" "+users.get(i).getId());
            }
                
        }catch(Exception e){
            e.printStackTrace();
        }
        User login = new User();
        login.setLogin("admin");
        login.setSenha("admin");
        boolean teste = false;
        try {
            teste = uc.efetuarLogin(login);
        } catch (CalcadoException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(teste);
    }
}
