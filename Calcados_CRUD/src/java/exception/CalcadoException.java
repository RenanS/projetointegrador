/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exception;

/**
 *
 * @author lucas.duffeck
 */
public class CalcadoException extends Exception{
    
    String erro;
    public CalcadoException(String erro){
        this.erro = erro;
    }
    
    public String showMessage(){
        return erro;
    }
}
