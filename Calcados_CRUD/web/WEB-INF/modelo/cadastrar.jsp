<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
    Document   : cadastrar
    Created on : 18/11/2016, 19:47:28
    Author     : lucas.duffeck
--%>

<%@page import="java.util.List"%>
<%@page import="model.Marca"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calçado</title>
    </head>
    <style>/*
        td, th {
            width: 4rem;
            height: 2rem;
            border: 1px solid #ccc;
            text-align: center;
        }
        th {
            background: lightblue;
            border-color: white;
        }
        body {
            padding: 1rem;
        }
        */
        .button {
            background-color: lightblue; /* Blue */
            border: none;
            padding: 12px 28px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
        }
    </style>
    <body>
        <h1>Cadastro de Calçado</h1>
        <form name="formCalcado">
            <table>
                <tr>
                    <th>
                        Nome
                    </th>
                    <td>
                        <input type="text" name="nome"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        Código
                    </th>
                    <td>
                        <input type="text" name="codigo"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        Marca 
                    </th>
                    <td>
                        <select name="marca">
                            <c:forEach var="marca" items="${marcas}">
                            <option value="${marca.id}">${marca.nome}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>
                        Modelo 
                    </th>
                    <td>
                        <select name="modelo">
                            <option value="1">Modelo 1</option>
                            <option value="2">Modelo 2</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button class="button" value="Salvar"/>Salvar</button>
                    </td>
                </tr>
                <tr>
                    <th>
                        Cor
                    </th>
                    <td>
                        <input type="text" name="cor"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        Descrição
                    </th>
                    <td>
                        <input type="text" name="descricao"/>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>

