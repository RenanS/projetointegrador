<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
    Document   : cadastrar.jsp
    Created on : 17/11/2016, 18:57:25
    Author     : lucas.duffeck
--%>
<%@page import="java.util.List"%>
<%@page import="model.Marca"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<script language='JavaScript'>
    function trocarUrl(){
        window.location.href="${param.getContextPath}/Calcados/Calcado";
    }
    function SomenteNumero(e){
        var tecla=(window.event)?event.keyCode:e.which;   
        if((tecla>47 && tecla<58)) return true;
        else{
            if (tecla===8 || tecla===0) return true;
            else  return false;
        }
}
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calçado</title>
    </head>
    <style>/*
        td, th {
            width: 4rem;
            height: 2rem;
            border: 1px solid #ccc;
            text-align: center;
        }
        th {
            background: lightblue;
            border-color: white;
        }
        body {
            padding: 1rem;
        }
        */
        .button {
            background-color: lightblue; /* Blue */
            border: none;
            padding: 12px 28px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
        }
        a {
            color: black;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
        .center {
            text-align: center;
        }
    </style>
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <body>
        <h1>Cadastro de Calçado</h1>
        ${mensagem}
        <form name="formCalcado" method="POST">
            <table>
                <input hidden id="id_atualizar" name="id_atualizar" value="${calcado.id}" />
                <tr>
                    <th>
                        Numeração 
                    </th>
                    <td>
                        <input id="numeracao" type="text" name="numeracao" value="${calcado.numeracao}" onkeypress='return SomenteNumero(event)'/>
                    </td>
                </tr>
               
                <tr>
                    <th>
                        Modelo 
                    </th>
                    <td>
                        <select name="modelo">
                            <c:forEach var="modelo" items="${modelos}">
                                <option ${calcado.modelo.id == modelo.id? 'selected' : ''} value="${modelo.id}">${modelo.nome} - ${modelo.cor}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button class="button" value="Salvar"/>Salvar</button>
                    </td>
                </tr>
            </table>
        </form>

        <p>Lista de Calçado Cadastrados</p>
        <table>
            <tr>
                <th class="center">
                    Id Calçado
                </th>
                <th>
                    Modelo
                </th>
                <th>
                    Cor
                </th>
                <th class="center">
                    Numeração
                </th>
                <th colspan="2" class="center">
                    Operações
                </th>                   
            </tr>
            <c:forEach var="calcado" items="${calcados}">
                <tr>
                    <td class="center">${calcado.id}</td>
                    <td>${calcado.modelo.nome}</td>
                    <td>${calcado.modelo.cor}</td>
                    <td class="center">${calcado.numeracao}</td>
                    <td value="${calcado.id}" class="center"><a href="${param.getContextPath}/Calcados/Calcado?id_calcado=${calcado.id}&excluir=N"<button class="button" >Editar</button></a></td>
                    <td value="${calcado.id}"class="center"><a href="/Calcados/Calcado?id_calcado=${calcado.id}&excluir=S"><button class="button">Excluir</button></a></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
