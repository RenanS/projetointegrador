<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
    Document   : cadastrar
    Created on : 18/11/2016, 20:11:41
    Author     : lucas.duffeck
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calçado</title>
    </head>
    <style>/*
        td, th {
            width: 4rem;
            height: 2rem;
            border: 1px solid #ccc;
            text-align: center;
        }
        th {
            background: lightblue;
            border-color: white;
        }
        body {
            padding: 1rem;
        }
        */
        .button {
            background-color: lightblue; /* Blue */
            border: none;
            padding: 12px 28px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
        }
    </style>
    <body>
        <c:choose>
            <c:when test="${!mensagem.isEmpty()}">
                <p style="color: blue">${mensagem}<p>
            </c:when>
            <c:when test="${mensagem.isEmpty()}">
            </c:when>
        </c:choose>
        <div></div>
        <h1>Cadastro de Categorias</h1>
        <form method="POST" name="formCategoria">
            <table>
                <tr>
                    <th>
                        Nome 
                    </th>
                    <td>
                        <input type="text" name="nome"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        Descrição
                    </th>
                    <td>
                        <input type="text" name="descricao"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button class="button" value="Salvar"/>Salvar</button>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>