<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
    Document   : logar
    Created on : 19/11/2016, 13:36:49
    Author     : Renan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style>
    .div {
        border-radius: 25px;
        border: 2px solid #191970;
        background: #27408B;
        padding: 20px;  
        width:500px; 
	height:200px; 
        position:absolute; 
        top:50%; 
        margin-top:-100px;
        left:50%;
        margin-left:-250px;
        text-align : center;
        color : white;
        align-content: center;
    }
    .button {
        background-color: #4CAF50; /* Green */
        border-radius: 12px;
        border: none;
        padding: 12px 28px;
        text-align: center;
        text-decoration: none;
        color: whitesmoke;
        display: inline-block;
        font-size: 12px;
    }
    .table{
        position: absolute;
        top: 34%;
        left: 50%;
        transform: translateX(-50%);
    }
    .erro{
        color: red;
    }
</style>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Efetuar Login</title>
    </head>
    <body>
        <c:if test="${!mensagem.isEmpty()}">
            <p class="erro">${mensagem}</p>
            </c:if>
        <div class="div">
            <h1>Efetuar Login</h1>
            <form name="formLogar" method="POST">
                <table class="table">
                    <tr>
                        <td>Login:</td>
                        <td><input type="text" name="login"id="login"/></td>
                    </tr>
                    <tr>
                        <td>Senha:</td>
                        <td><input type="password" name="senha" id="senha"/></td>
                    </tr>
                    <tr>
                        <td colspan="2"><button class="button" value="Entrar"/>Entrar</button></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
